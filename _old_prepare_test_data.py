import pandas as pd
import cv2 as cv
import random
from pathlib2 import Path
from shutil import copyfile


initial_images_folder = 'data/celeba/img_align_celeba/'
test_data_final_folder = 'data/test_data/'
attributes_file_path = 'data/celeba/list_attr_celeba.csv'
number_of_examples_per_class = 100


positive_images_folder = test_data_final_folder + 'positives/images/'
negative_images_folder = test_data_final_folder + 'negatives/images/'
Path(positive_images_folder).mkdir(parents = True, exist_ok = True)
Path(negative_images_folder).mkdir(parents = True, exist_ok = True)

def prepare_samples(attr, image_saving_folder):
	file_names = []
	for i in range(number_of_examples_per_class):
		file_names.append(attr.iloc[random.randint(0, number_of_examples_per_class)]['image_id'])
	[copyfile(initial_images_folder + file_name, image_saving_folder + file_name) for file_name in file_names]

def prepare_training_data():
	attr = pd.read_csv(attributes_file_path)
	prepare_samples(attr[attr['Eyeglasses'] == 1], positive_images_folder)
	prepare_samples(attr[attr['Eyeglasses'] == -1], negative_images_folder)

if __name__ == "__main__":
	prepare_training_data()