import cv2 as cv
from logics.my_classes.EyeglassesDetector import EyeglassesDetector as eyeglasses_detector

photos_absolute_paths_file_path = 'input/photos_absolute_paths.txt'

def detect_eyeglasses():
	try:
		with open(photos_absolute_paths_file_path) as file:
			filenames = [filename.strip() for filename in file.readlines()]
	except Exception as e:
		print(e)
		raise Exception('photos_absolute_paths.txt is not filled correctly')

	try:
		images = [cv.imread(filename) for filename in filenames]
	except Exception as e:
		print(e)
		raise Exception('cannot open image by photos_absolute_paths.txt specified path')

	labels = eyeglasses_detector().detect(images)
	for idx, label in enumerate(labels):
		if label:
			print('Glasses detected: {}'.format(filenames[idx]))
		else:
			print('Glasses not detected: {}'.format(filenames[idx]))
	# detected_indices = [idx for idx, el in enumerate(labels) if el == True]
	# not_detected_indices = [idx for idx, el in enumerate(labels) if el == False]
	# print('Glasses detected:')
	# [print(filenames[idx]) for idx in detected_indices]
	# print('Glasses not detected:')
	# [print(filenames[idx]) for idx in not_detected_indices]

if __name__ == "__main__":
	detect_eyeglasses()