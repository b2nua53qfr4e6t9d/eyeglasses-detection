# Prepared data folder
prep_data="./data/test_data"
valid_data="./data/valid_data"
# One eights
valid_split=8
# Auto generated paths
positives="$prep_data/positives"
negatives="$prep_data/negatives"
pos_images="$positives/images"
neg_images="$negatives/images"
val_pos_images="$valid_data/positives/images"
val_neg_images="$valid_data/negatives/images"
# Count numbers
pos_num=0
for f_path in "$pos_images"/*.*; do
	let "pos_num += 1"
done
neg_num=0
for f_path in "$neg_images"/*.*; do
	let "neg_num += 1"
done
i=0
#naaah i have not time, so
exit 1
for f_path in "$pos_images"/*.*; do
	let "i += 1"
	if [ $i -gt $((pos_num - pos_num / valid_split)) ]; then
		f_name=${f_path##*/}
		cp $f_path $val_pos_images
		echo $i
	fi
done