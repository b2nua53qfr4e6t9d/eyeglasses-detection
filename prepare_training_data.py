import pandas as pd
import cv2 as cv
from pathlib2 import Path
from logics.my_classes.CelebaImageSelector import CelebaImageSelector as image_selector
from logics.my_classes.FaceNormalizer import FaceNormalizer as face_normalizer


initial_images_folder = 'data/celeba/img_align_celeba/'
prepared_data_final_folder = 'data/prepared_data/'
attributes_file_path = 'data/celeba/list_attr_celeba.csv'
face_landmarks_predictor_path = 'models/off-the-shelp_models/shape_predictor_68_face_landmarks.dat'

examples_per_combination_pos = 32
examples_per_combination_neg = 32

positive_images_folder = prepared_data_final_folder + 'positives/images/'
negative_images_folder = prepared_data_final_folder + 'negatives/images/'
Path(positive_images_folder).mkdir(parents = True, exist_ok = True)
Path(negative_images_folder).mkdir(parents = True, exist_ok = True)

form_im_name = lambda im: '{}_{}_{}'.format(im['group_number'], im['features_presence'], im['image_id'])
face_norm = face_normalizer(face_landmarks_predictor_path)

def prepare_samples(attr, image_saving_folder, examples_per_combination = 1):
	print('Preparing samples to save in {}'.format(image_saving_folder))
	print('This may take {} minutes. Or may not'.format((examples_per_combination_pos + examples_per_combination_neg) * 1.25 * 2))
	images = image_selector(attr, examples_per_combination = examples_per_combination).select_images()
	[im.update({'image' : cv.imread(initial_images_folder + im['image_id'])}) for im in images]
	[im.update({'image' : face_norm.normalize_image(im['image'])}) for im in images]
	[cv.imwrite(image_saving_folder + form_im_name(im), im['image']) for im in images if not im['image'] is None]

def prepare_training_data():
	attr = pd.read_csv(attributes_file_path)
	prepare_samples(attr[attr['Eyeglasses'] == 1], positive_images_folder, examples_per_combination = examples_per_combination_pos)
	prepare_samples(attr[attr['Eyeglasses'] == -1], negative_images_folder, examples_per_combination = examples_per_combination_neg)

if __name__ == "__main__":
	prepare_training_data()