import pandas as pd
import cv2 as cv
import re
from pathlib2 import Path
from os import listdir
from os.path import isfile, join
from logics.my_classes.CelebaImageSelector import CelebaImageSelector as image_selector
from logics.my_classes.FaceNormalizer import FaceNormalizer as face_normalizer
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
from keras.models import load_model



photos_absolute_paths_file_path = 'input/photos_absolute_paths.txt'
# initial_images_folder = 'input/photos/'
prepared_data_final_folder = 'input/prepared_photos/unknown_class/'
face_landmarks_predictor_path = 'models/off-the-shelp_models/shape_predictor_68_face_landmarks.dat'

Path(prepared_data_final_folder).mkdir(parents = True, exist_ok = True)

face_norm = face_normalizer(face_landmarks_predictor_path)

def prepare_samples(file_names, file_paths, image_saving_folder):
	images = [{'image_id' : fn} for fn in file_names]
	[im.update({'image_path' : file_path}) for file_path in file_paths]
	[im.update({'image' : cv.imread(im['image_path'])}) for im in images]
	[im.update({'image' : face_norm.normalize_image(im['image'])}) for im in images]
	[cv.imwrite(image_saving_folder + im['image_id'], im['image']) for im in images if not im['image'] is None]
	[print("{} : no face detected".format(im['image_path'])) for im in images if im['image'] is None]

def load_files_info():
	try:
		with open(photos_absolute_paths_file_path) as file:
			file_paths = [file_path.strip() for file_path in file.readlines()]
	except Exception as e:
		print(e)
		raise Exception('photos_absolute_paths.txt is not filled correctly')
	file_names = [re.findall(r'[^\\/:*?"<>|\r\n]+$', file_path)[0] for file_path in file_paths]

	return file_names, file_paths
	

def predict_glasses():
	model=load_model('models/my_trained_models/cnn.h5')
	w = 32
	h = 21
	mult = 4
	w *= mult
	h *= mult
	batch_size = 16
	test_datagen = ImageDataGenerator()

	test_generator = test_datagen.flow_from_directory(
	        'input/prepared_photos/',
	        target_size=(w, h),
	        color_mode="rgb",
	        shuffle = False,
	        class_mode='binary',
	        batch_size=1)

	filenames = test_generator.filenames
	nb_samples = len(filenames)

	predict = model.predict_generator(test_generator,steps = nb_samples)

	predict = [bool(el > 0.9) for el in predict]
	print(predict)
	for idx, filename in enumerate(filenames):
	    if predict[idx]:
	        print("{} has glasses".format(filename))
	    else:
	        print("{} has no glasses".format(filename))

if __name__ == "__main__":
	file_names, file_paths = load_files_info()
	prepare_samples(file_names, file_paths, prepared_data_final_folder)
	predict_glasses()