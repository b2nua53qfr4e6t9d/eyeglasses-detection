# Eyeglasses detection

This repo contains code that allows you to classify whether the photo  
contains face with the glasses, or without them.  
Following methods were used:  
* Opencv Haar cascade  
* CNN (it gave about *0.9 accuracy*)  

# Dependencies
* python (was tested on 2.7) 
* python-opencv
* dlib
* tensorflow
* keras
* side-packages like numpy and so on, which are likely to be installed automatically

# How to peform detection
1. You clone the repo
2. You put your photos to the input  
   * You put your photos in the *./input/photos*  
   * In console change directory to the folder of the copied repo, do > *cd input*  
   * do > *./fill_paths.sh*  
3. From the repo directory, run > *python detect_eyeglasses.py*  

(If you need it, idk for what kind of reason, you can just place full paths  
to your pics in the *input/photos_absolute_paths.txt*)

# How to train
1. Complete faces dataset with pictures by placing *img_align_celeba* folder with images  which you can [get on Kaggle](https://www.kaggle.com/jessicali9530/celeba-dataset)
2. Run > *python prepare_training_data.py*
3. Cut and paste (!sic) part of the images from generated *data/prepared_data* to the same-structured folder *data/valid_data*. Yes, I know I could have written a shell script. I could.
4. Train CNN straightforwardly using *CNN-train.ipynb*. You can open it with jupyter or ipython notebook

# Some warnings
* Run all the scripts from their directory
* You need not to forget that the files we detect glasses in, are set by *input/photos_absolute_paths.txt*
* I peform face detection and normalization using *pre-trained opencv face landmark detector*. It is pretty heavyweight