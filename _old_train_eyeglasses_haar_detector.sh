# Prepared data folder
prep_data="./data/prepared_data"
mergevec="./logics/side_libs/opencv-haar-classifier-training/tools/mergevec.py"
num=4
w=32
h=32
bgthresh=10
maxxangle=0.0
maxyangle=0.0
maxzangle=0.1
maxidev=20
# Auto generated paths
positives="$prep_data/positives"
negatives="$prep_data/negatives"
pos_images="$positives/images"
neg_images="$negatives/images"
pos_txt="$positives/positives.txt"
neg_txt="$negatives/negatives.txt"
samples="$prep_data/samples"
combined_samples="$samples/combined_samples.vec"
trained_models="./models/my_trained_models"
# Generate filename list files from images
find $pos_images -iname "*.jpg" > $pos_txt
find $neg_images -iname "*.jpg" > $neg_txt
# Create .vec files from positive images
number_of_pictures=0
for f_path in "$pos_images"/*.*; do
	let "number_of_pictures += 1"
    f_name=${f_path##*/}
    echo $f_path
    opencv_createsamples -vec "$samples/$f_name.vec" -img $f_path -num $num -w $w -h $h \
    -bgthresh $bgthresh -maxxangle $maxxangle -maxyangle $maxyangle -maxzangle $maxzangle -maxidev $maxidev
done
# Calculate some variables
echo "Total number of used pictures: $number_of_pictures"
let "number_of_pos_samples=$num*$number_of_pictures"
echo "Number of positive samples generated: $number_of_pos_samples"
number_of_negatives=0
for f_path in "$neg_images"/*.*; do
	let "number_of_negatives += 1"
done
echo "Number of negative images avaliable (all of them will be used): $number_of_negatives"
# Merge .vec files into one
python $mergevec -v $samples -o $combined_samples
opencv_traincascade -data $trained_models \
-vec $combined_samples \
-bg $neg_txt \
-numStages 20 -minHitRate 0.99 -maxFalseAlarmRate 0.5 -numPos $((number_of_pos_samples / 4 * 3)) \
-numNeg $((number_of_negatives / 8 * 7)) -w $w -h $h -mode ALL -precalcValBufSize 16384 \
-precalcIdxBufSize 16384
#$(($w * $h))


# create .vec files from images
### tweak parameters
# DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
# positives="./data/prepared_data/positives/images"
# for f in "$positives"/*.*; do
#     # do some stuff here with "$f"
#     f_name=${f##*/}
#     echo "$positives/$f_name.vec"
#     opencv_createsamples -vec "$positives/$f_name.vec" -img $f -num 1 -bgthresh 10 -maxxangle 0.0 -maxyangle 0.0 -maxzangle 0.2 -maxidev 20
#     # remember to quote it or spaces may misbehave
# done
# dir="/home/lotkov/coding/faces/eyeglass-recog/data/prepared_data/positives"
# echo "$dir"
# for file in "$dir"/*
# do
#     echo $file
# done
# exit 1
# 	opencv_createsamples -vec asdf.vec -img $f -pngoutput -bgcolor 10 -bgthresh 10 -maxxangle 0.0 -maxyangle 0.0 -maxzangle 0.2 -maxidev 20 -w 128 -h 128
# python ./logics/side_libs/opencv-haar-classifier-training/tools/mergevec.py -v vecs/ -o com.vec
#-bg ./data/prepared_data/negatives/negatives.txt
#perl ./logics/side_libs/opencv-haar-classifier-training/bin/createsamples.pl "/home/lotkov/coding/faces/eyeglass-recog/data/prepared_data/positives.txt" ./data/prepared_data/negatives/negatives.txt ./data/prepared_data/samples 15 "opencv_createsamples -bgcolor 0 -bgthresh 0 -maxxangle 1.1 -maxyangle 1.1 maxzangle 0.5 -maxidev 40 -w 80 -h 40"
#perl ./logics/side_libs/opencv-haar-classifier-training/bin/createsamples.pl \
#data/prepared_data/positives/positives.txt\
#./data/prepared_data/negatives/negatives.txt\
#./data/prepared_data/samples 15\
### tweak parameters
#  "opencv_createsamples -bgcolor 0 -bgthresh 0 -maxxangle 1.1\
#  -maxyangle 1.1 maxzangle 0.5 -maxidev 40 -w 80 -h 40"
#exit 1
# merge .vec files
# python ./logics/side_libs/opencv-haar-classifier-training/tools/mergevec.py \
# -v ./data/prepared_data/samples \
# -o ./data/prepared_data/combined_samples.vec
# training cascade
### tweak parameters
#opencv_traincascade -data ./models/my_trained_models \
#-vec ./data/prepared_data/combined_samples.vec \
#-bg ./data/prepared_data/negatives/negatives.txt \
#-numStages 20 -minHitRate 0.999 -maxFalseAlarmRate 0.5 -numPos 1000\
#-numNeg 600 -w 80 -h 40 -mode ALL -precalcValBufSize 1024\
#-precalcIdxBufSize 1024
# /home/lotkov/coding/faces/eyeglass-recog/data/prepared_data/positives.txt