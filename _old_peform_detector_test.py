import cv2 as cv
from os import listdir
from os.path import isfile, join
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from logics.my_classes.EyeglassesDetector import EyeglassesDetector as eyeglasses_detector


test_data_folder = 'data/test_data/'


positive_images_folder = test_data_folder + 'positives/images/'
negative_images_folder = test_data_folder + 'negatives/images/'

def test_class(image_folder):
	filenames = [f for f in listdir(image_folder) if isfile(join(test_data_path, f))]
	images = [cv.imread(test_data_path + filename) for filename in filenames]
	labels = eyeglasses_detector().detect(images)

def test_detector():
	positives = test_class(positive_images_folder)
	negatives = test_class(negative_images_folder)
	y = positives + negatives
	true = [True for i in range(len(positives))]
	false = [False for i in range(len(negatives))]
	t = true + false

	print('accuracy_score: {}'.format(accuracy_score(t, y)))
	print('precision_score: {}'.format(precision_score(t, y)))
	print('recall_score: {}'.format(recall_score(t, y)))
	print('f1_score: {}'.format(f1_score(t, y)))


if __name__ == "__main__":
	test_detector()