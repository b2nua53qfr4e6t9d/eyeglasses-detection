import pandas as pd
import numpy as np
import cv2 as cv
import dlib
from imutils.face_utils import FaceAligner
from imutils.face_utils import rect_to_bb
import imutils

attr = pd.read_csv('celeba/list_attr_celeba.csv')

df_glasses = attr[attr['Eyeglasses'] == 1]
df_no_glasses = attr[attr['Eyeglasses'] == -1]

im_folder = 'celeba/img_align_celeba/'
predictor_path = 'shape_predictor_68_face_landmarks.dat'
important_features = [
	'Arched_Eyebrows',
	'Bags_Under_Eyes',
	'Bangs',
	'Bushy_Eyebrows',
	'Wearing_Hat'
]
examples_per_combination_glasses = 2
examples_per_combination_no_glasses = 4


def select_various_images(df, features_to_pick, examples_per_combination):
	print('select_various_images')
	group_number = 0
	images = []
	for i, feature_1 in enumerate(features_to_pick):
		for j, feature_2 in enumerate(features_to_pick):
			if feature_1 == feature_2:
				continue
			for enable_1 in [-1, 1]:
				for enable_2 in [-1, 1]:
					group_number += 1
					selected_df = df[(df[feature_1] == enable_1) & (df[feature_2] == enable_2)]
					if selected_df.empty:
						print(feature_1, feature_2, enable_1, enable_2)
						continue

					example_number = 0
					row_to_choose = 0
					while example_number < examples_per_combination:
						try:
							if selected_df[row_to_choose:].empty:
								break
							image_id = selected_df[row_to_choose:].iloc[0]['image_id']
							row_to_choose += 1
							if image_id in set([el[1] for el in images]):
								continue
							# print(example_number, image_id)
							images.append((group_number, image_id))
							example_number += 1
						except Exception as e:
							print(e)
							pass
	return images

def align_and_save(images, saving_path, predictor_path):
	print('align_and_save')
	for image_number, (group_number, image_id) in enumerate(images):
		print(str(image_number) + " out of " + str(len(images)))
		image = cv.imread(im_folder + str(image_id))
		try:
			detector = dlib.get_frontal_face_detector()
			predictor = dlib.shape_predictor(predictor_path)
			fa = FaceAligner(predictor, desiredFaceWidth=256)

			# load the input image, resize it, and convert it to grayscale
			image = imutils.resize(image, width=800)
			gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)

			rects = detector(gray, 2)

			# loop over the face detections
			for rect in rects:
				# extract the ROI of the *original* face, then align the face
				# using facial landmarks
				(x, y, w, h) = rect_to_bb(rect)
				width = 256
				faceOrig = imutils.resize(image[y:y + h, x:x + w], width=width)
				faceAligned = fa.align(image, gray, rect)
				faceAligned = faceAligned[:int(-width / 3), :]

				cv.imwrite(saving_path + str(group_number) + '_' + image_id + ".png", faceAligned)

				# display the output images
				# cv.imshow("Original", faceOrig)
				# cv.imshow("Aligned", faceAligned)
				# cv.waitKey(0)
		except Exception as e:
			print(e)
			exit(0)

images_glasses = select_various_images(df_glasses, important_features, examples_per_combination_glasses)
align_and_save(images_glasses, 'prepared_data/with_glasses/', predictor_path)

images_no_glasses = select_various_images(df_no_glasses, important_features, examples_per_combination_no_glasses)
align_and_save(images_no_glasses, 'prepared_data/without_glasses/', predictor_path)

exit(0)