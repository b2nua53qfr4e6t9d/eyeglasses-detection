import cv2 as cv
from logics.my_classes.FaceNormalizer import FaceNormalizer as face_normalizer

face_landmarks_predictor_path = 'models/off-the-shelp_models/shape_predictor_68_face_landmarks.dat'

class EyeglassesDetector():
	def __init__(self, cascade_file_path = 'models/my_trained_models/cascade.xml'):
		print(self.__class__.__name__, 'inited')
		self.cascade = cv.CascadeClassifier(cascade_file_path)
		self.face_normalizer = face_normalizer(face_landmarks_predictor_path)

		self.labels = []

	def detect(self, images):
		normalized_images = []
		for image in images:
			normalized_images.append(self.face_normalizer.normalize_image(image))
		for im in normalized_images:
			if not im is None:
				detection = self.cascade.detectMultiScale(im)
			if len(detection):
				self.labels.append(True)
			else:
				self.labels.append(False)
		return self.labels