import dlib
import imutils
import cv2 as cv
from imutils.face_utils import FaceAligner
from imutils.face_utils import rect_to_bb

class FaceNormalizer():
	def __init__(self, predictor_path):
		print(self.__class__.__name__, 'inited')
		self.initial_resize_width = 800
		self.desired_face_width = 256
		self.bottom_crop_part = 88

		self.detector = dlib.get_frontal_face_detector()
		self.predictor = dlib.shape_predictor(predictor_path)
		self.face_aligner = FaceAligner(self.predictor, desiredFaceWidth=self.desired_face_width)

		self.normalized_images = []


	def normalize_image(self, image):
		normalized_image = None
		image = imutils.resize(image, width=self.initial_resize_width)
		gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
		rects = self.detector(gray, 2)
		rect = self.select_biggest_rectangle(rects)
		if not rect is None:
			normalized_image = self.face_aligner.align(image, gray, rect)
			normalized_image = normalized_image[:int(-self.bottom_crop_part), :]
		return normalized_image

	def select_biggest_rectangle(self, rects):
		biggest_rect = None
		try:
			areas = [rect.area() for rect in rects]
			biggest_rect = rects[areas.index(max(areas))]
		except:
			pass
		return biggest_rect





	# def normalize_images(self, images):
	# 	print('normalize_images')
	# 	for num_image_processed, image in enumerate(images):
	# 		print('{} out of {} images processed'.format(num_image_processed, len(images)))
	# 		try:
	# 			# load the input image, resize it, and convert it to grayscale
	# 			image = imutils.resize(image, width=self.initial_resize_width)
	# 			gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
	# 			rects = self.detector(gray, 2)
	# 			print(rects)
	# 			exit(0)
	# 			# loop over the face detections
	# 			for rect in rects:
	# 				# extract the ROI of the *original* face, then align the face
	# 				# using facial landmarks
	# 				# (x, y, w, h) = rect_to_bb(rect)
	# 				# faceOrig = imutils.resize(image[y:y + h, x:x + w], width=self.desired_face_width)
	# 				face_aligned = self.face_aligner.align(image, gray, rect)
	# 				face_aligned = face_aligned[:int(-self.desired_face_width * self.bottom_crop_part), :]
	# 				self.normalized_images.append(face_aligned)
	# 		except Exception as e:
	# 			print(e)
	# 			exit(0)
	# 	return self.normalized_images
