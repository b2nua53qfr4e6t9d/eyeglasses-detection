class CelebaImageSelector():
	def __init__(self, attributes_dataframe, examples_per_combination = 2, important_features = None):
		print(self.__class__.__name__, 'inited')
		self.attr_df = attributes_dataframe
		self.examples_per_combination = examples_per_combination
		self.important_features = [
			'Arched_Eyebrows',
			'Bags_Under_Eyes',
			'Bangs',
			'Bushy_Eyebrows',
			'Wearing_Hat'
		]
		if not important_features is None:
			self.important_features = important_features

		self.images = []

	def select_images(self):
		group_number = 0
		for first_feature in self.important_features:
			for second_feature in self.important_features:
				if first_feature == second_feature:
					continue
				for is_first_enabled in [-1, 1]:
					for is_second_enabled in [-1, 1]:
						group_number += 1
						selected_df = self.attr_df[(self.attr_df[first_feature] == is_first_enabled) & (self.attr_df[second_feature] == is_second_enabled)]
						if selected_df.empty:
							continue
						example_number = 0
						row_to_choose = 0
						while example_number < self.examples_per_combination:
							try:
								if selected_df[row_to_choose:].empty:
									break
								image_id = selected_df[row_to_choose:].iloc[0]['image_id']
								row_to_choose += 1
								if image_id in set([el['image_id'] for el in self.images]):
									continue
								self.images.append({
									'group_number' : group_number,
									'features_presence' : '{}_{}_{}_{}'.format(first_feature, second_feature, is_first_enabled, is_second_enabled),
									'image_id' : image_id
									})
								example_number += 1
							except:
								pass
		return self.images